﻿using UnityEngine;

public class DesctopInputModule : InputModule
{
    public override void Refresh(float dt)
    {
        if (Input.GetKeyDown(KeyCode.W))
        {
            Move(Vector2.up);
        }

        if (Input.GetKeyDown(KeyCode.S))
        {
            Move(Vector2.down);
        }

        if (Input.GetKeyDown(KeyCode.D))
        {
            Move(Vector2.right);
        }

        if (Input.GetKeyDown(KeyCode.A))
        {
            Move(Vector2.left);
        }

        if (Input.GetKeyDown(KeyCode.Z))
        {
            Scale(1f);
        }

        if (Input.GetKeyDown(KeyCode.X))
        {
            Scale(-1f);
        }

        if (Input.GetKeyDown(KeyCode.C))
        {
            Scale(200f);
        }

        if (Input.GetKeyDown(KeyCode.V))
        {
            Scale(-200f);
        }
    }
}