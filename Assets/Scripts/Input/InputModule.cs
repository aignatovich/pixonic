﻿using System;
using UnityEngine;

public abstract class InputModule
{
    public event Action<Vector2> OnMove;
    public event Action<int> OnScale;

    public abstract void Refresh(float dt);

    protected void Move(Vector2 dir)
    {
        if (OnMove != null)
        {
            OnMove(dir);
        }
    }

    protected void Scale(float value)
    {
        if (OnScale != null)
        {
            OnScale((int)value);
        }
    }

    public static InputModule Create()
    {
        // Todo: switch platform
        return new DesctopInputModule();
    }
}
