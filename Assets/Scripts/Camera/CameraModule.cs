﻿using UnityEngine;

public class CameraModule : MonoBehaviour
{
    [SerializeField] private float height = 10;

    IGameContext gameContext;

    public void Init(IGameContext gameContext)
    {
        this.gameContext = gameContext;

        gameContext.OnShipPositionChanged += OnShipPositionChanged;
    }

    void OnDestroy()
    {
        gameContext.OnShipPositionChanged -= OnShipPositionChanged;
    }

    void OnShipPositionChanged(int x, int y, int zoom)
    {
        Camera.main.orthographicSize = zoom / 2f;
        if (gameContext.Entities.Ship != null)
        {
            transform.position = new Vector3(gameContext.Entities.Ship.transform.position.x, gameContext.Entities.Ship.transform.position.y, gameContext.Entities.Ship.transform.position.z - height);
        }
    }
}
