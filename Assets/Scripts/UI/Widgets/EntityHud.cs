﻿using UnityEngine;
using UnityEngine.UI;

public class EntityHud : MonoBehaviour
{
    [SerializeField] Text scores;

    public void Init(int scores)
    {
        this.scores.text = scores.ToString();
    }
}
