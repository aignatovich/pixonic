﻿using UnityEngine;
using UnityEngine.UI;

public class LevelScreen : UIScreen
{
    [SerializeField] private Text zoomLbl;
    [SerializeField] private Text positionLbl;

    IGameContext gameContext;

    public void Init(IGameContext gameContext)
    {
        this.gameContext = gameContext;

        gameContext.OnShipPositionChanged += OnShipPositionChanged;
    }

    public override void Hide()
    {
        gameContext.OnShipPositionChanged -= OnShipPositionChanged;

        base.Hide();
    }

    void OnShipPositionChanged(int x, int y, int zoom)
    {
        zoomLbl.text = string.Format("ZOOM: {0}", zoom);
        positionLbl.text = string.Format("X: {0} Y: {1}", x, y);
    }
}
