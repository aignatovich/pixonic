﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hud : MonoBehaviour
{
    Dictionary<int, EntityHud> entitiesHuds;
    IGameContext gameContext;

    public void Show(IGameContext gameContext)
    {
        this.gameContext = gameContext;

        entitiesHuds = new Dictionary<int, EntityHud>();

        gameContext.OnShipPositionChanged += OnShipPositionChanged;
        gameContext.Entities.OnEntityAdded += OnEntityAdded;
        gameContext.Entities.OnEntityRemoved += OnEntityRemoved;
    }

    public void Hide()
    {
        gameContext.OnShipPositionChanged -= OnShipPositionChanged;
        gameContext.Entities.OnEntityAdded -= OnEntityAdded;
        gameContext.Entities.OnEntityRemoved -= OnEntityRemoved;
    }

    void OnShipPositionChanged(int x, int y, int zoom)
    {
        StartCoroutine(RefresEntitieshHuds());
    }

    IEnumerator RefresEntitieshHuds()
    {
        yield return null;

        foreach (var hud in entitiesHuds)
        {
            Entity entity = gameContext.Entities.GetEntity(hud.Key);
            hud.Value.transform.position = Camera.main.WorldToScreenPoint(entity.transform.position);
        }
    }

    void OnEntityAdded(int entityId)
    {
        Entity entity = gameContext.Entities.GetEntity(entityId);
        Vector3 pos = Camera.main.WorldToScreenPoint(entity.transform.position);
        EntityHud entityHud = gameContext.Pool.Spawn<EntityHud>(Constants.PathToEntityHudPrefab, pos, 0);
        entityHud.gameObject.transform.SetParent(transform);
        entityHud.Init(entity.Score);
        entitiesHuds.Add(entityId, entityHud);
    }

    void OnEntityRemoved(int entityId)
    {
        EntityHud hud;
        if (entitiesHuds.TryGetValue(entityId, out hud))
        {
            gameContext.Pool.Despawn(Constants.PathToEntityHudPrefab, hud.gameObject);
            entitiesHuds.Remove(entityId);
        }
        else
        {
            throw new System.Exception("Cant find hud for entity " + entityId);
        }
    }
}
