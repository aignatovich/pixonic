﻿using System.Collections.Generic;
using System;
using UnityEngine;

public class UIManager
{
    UIScreen currentScreen;
    Dictionary<Type, string> mediatorMap;
    GameObject uiRoot;
    Hud hud;

    public UIManager()
    {
        CreateRootObject();
        InitGUIMap();
    }

    public void HideCurrentScreen()
    {
        if (currentScreen != null)
        {
            currentScreen.Hide();
            GameObject.Destroy(currentScreen.gameObject);
        }
    }

    public T ShowScreen<T>() where T : UIScreen
    {
        if (currentScreen != null)
        {
            currentScreen.Hide();
            GameObject.Destroy(currentScreen.gameObject);
        }

        GameObject prefab = Resources.Load(mediatorMap[typeof(T)]) as GameObject;
        currentScreen = (GameObject.Instantiate(prefab) as GameObject).GetComponent<T>();
        RectTransform prefabTransform = prefab.transform as RectTransform;
        RectTransform screenTransform = currentScreen.transform as RectTransform;
        screenTransform.SetParent(uiRoot.transform);
        screenTransform.localPosition = Vector3.zero;
        screenTransform.sizeDelta = prefabTransform.sizeDelta;
        currentScreen.Show();
        return currentScreen as T;
    }

    void CreateRootObject()
    {
        var rootPrefab = Resources.Load<GameObject>("Prefabs/UI/UIRoot");
        uiRoot = GameObject.Instantiate(rootPrefab);
        GameObject.DontDestroyOnLoad(uiRoot);
    }

    public void LoadHud(IGameContext gameContext)
    {
        var hoodPrefab = Resources.Load<GameObject>("Prefabs/UI/Hud");
        hud = GameObject.Instantiate(hoodPrefab).GetComponent<Hud>();
        hud.transform.SetParent(uiRoot.transform);
        ((RectTransform)hud.transform).localPosition = Vector3.zero;
        ((RectTransform)hud.transform).sizeDelta = Vector2.zero;
        hud.Show(gameContext);
    }

    void InitGUIMap()
    {
        mediatorMap = new Dictionary<Type, string>();
        mediatorMap.Add(typeof(LevelScreen), "Prefabs/UI/Screens/LevelScreen");
    }
}
