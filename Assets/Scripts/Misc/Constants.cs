﻿public static class Constants
{
    #region map
    public const float MinPlanetChance = 0.3f;
    public const float MaxPlanetChance = 0.5f;
    public const int MinZoom = 5;
    public const int SpecialModeZoom = 10;
    public const int MaxZoom = 10000;
    public const int MaxPlanetVisible = 20;
    #endregion

    #region rating
    public const int MinRating = 0;
    public const int MaxRating = 10000;
    #endregion

    #region path_to_prefabs
    public const string PathToEntityHudPrefab = "Prefabs/Ui/Widgets/EntityHud";
    public const string PathToShipPrefab = "Prefabs/Ships/Ship";
    public const string SpaceCellPrefabPath = "Prefabs/Cells/Cell";
    public static readonly string[] PlanetPrefabsPathes = new string[]
    {
        "Prefabs/Planets/Planet1",
        "Prefabs/Planets/Planet2",
        "Prefabs/Planets/Planet3",
        "Prefabs/Planets/Planet4",
        "Prefabs/Planets/Planet5",
    };
    #endregion
}
