﻿using System;

public interface IGameContext
{
    event Action<int, int, int> OnShipPositionChanged;

    Map Map { get; }
    GameObjectsPool Pool { get; }
    Entities Entities { get; }
    int Zoom { get; }
}
