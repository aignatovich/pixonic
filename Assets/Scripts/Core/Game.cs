﻿using System;
using UnityEngine;

public class Game : MonoBehaviour, IGameContext
{
    #region IGameContext
    public event Action<int, int, int> OnShipPositionChanged;

    public Map Map { get; private set; }
    public GameObjectsPool Pool { get; private set; }
    public Entities Entities { get; private set; }
    public int Zoom { get; private set; }
    #endregion

    InputModule playerInput;
    CameraModule cameraModule;
    UIManager uIManager;

    void Start()
    {
        Zoom = Constants.MinZoom;

        Pool = new GameObjectsPool();
        cameraModule = FindObjectOfType<CameraModule>();
        playerInput = InputModule.Create();
        playerInput.OnMove += OnMove;
        playerInput.OnScale += OnScale;
        IdGenerator idGenerator = new IdGenerator();
        Map = new Map();

        Entities = new Entities(this, idGenerator);
        uIManager = new UIManager();
        var levelScreen = uIManager.ShowScreen<LevelScreen>();
        levelScreen.Init(this);
        uIManager.LoadHud(this);

        Entities.CreateShip();
        cameraModule.Init(this);
        Refresh();
    }

    void Update()
    {
        playerInput.Refresh(Time.deltaTime);
    }

    #region userinput
    void OnMove(Vector2 dir)
    {
        Entities.MoveShip(dir);
        Refresh();
    }

    void OnScale(int value)
    {
        Zoom = Mathf.Clamp(Zoom + value, Constants.MinZoom, Constants.MaxZoom);
        Refresh();
    }
    #endregion

    void Refresh()
    {
        Map.Refresh(Zoom, Entities.Ship.PosX, Entities.Ship.PosY, Entities.Ship.Score);
        OnShipPositionChanged((int)Entities.Ship.transform.position.x, (int)Entities.Ship.transform.position.y, Zoom);
    }

    private void OnDestroy()
    {
        playerInput.OnMove -= OnMove;
        playerInput.OnScale -= OnScale;
    }
}
