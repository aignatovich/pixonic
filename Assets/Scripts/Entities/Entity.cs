﻿using UnityEngine;

public class Entity : MonoBehaviour
{
    public int Id { get; protected set; }
    public string PathToPrefab { get; protected set; }
    public int Score { get; private set; }
    public int PosX { get { return (int)transform.position.x; } }
    public int PosY { get { return (int)transform.position.y; } }

    public virtual void Init(int id, string pathToPrefab, int scores)
    {
        Id = id;
        PathToPrefab = pathToPrefab;
        Score = scores;
    }

    public void Move(Vector2 dir)
    {
        transform.position += new Vector3(dir.x, dir.y, 0);
    }
}
