﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class Entities
{
    public Entity Ship { get { return entities[shipId]; } }
    public event Action<int> OnEntityAdded;
    public event Action<int> OnEntityRemoved;

    IGameContext battleContext;
    IdGenerator idGenerator;
    GameObject root;
    Dictionary<int, Entity> entities;
    GameObject space;
    int shipId;

    public Entities(IGameContext battleContext, IdGenerator idGenerator)
    {
        this.battleContext = battleContext;
        this.idGenerator = idGenerator;

        root = new GameObject("Entities");
        entities = new Dictionary<int, Entity>();

        battleContext.OnShipPositionChanged += OnShipPositionChanged;
        battleContext.Map.OnShowPlanet += OnShowPlanet;
        battleContext.Map.OnHidePlanet += OnHidePlanet;

        space = battleContext.Pool.Spawn(Constants.SpaceCellPrefabPath);
        space.transform.position = new Vector3(0, 0);
    }

    public void Finish()
    {
        battleContext.OnShipPositionChanged -= OnShipPositionChanged;
        battleContext.Map.OnShowPlanet -= OnShowPlanet;
        battleContext.Map.OnHidePlanet -= OnHidePlanet;
    }

    void OnShipPositionChanged(int x, int y, int zoom)
    {
        space.transform.localScale = new Vector3(zoom, zoom);
        space.transform.position = new Vector3(x, y);

        float scale = battleContext.Zoom > Constants.SpecialModeZoom ? battleContext.Zoom / Constants.SpecialModeZoom : 1f;
        Vector3 scaleVector = new Vector3(scale, scale, scale);
        foreach (var entity in entities)
        {
            entity.Value.transform.localScale = scaleVector;
        }
    }

    void OnShowPlanet(int x, int y, int score, string prefab)
    {
        Entity planet = GetPlanetByPosition(x, y);
        if (planet != null)
        {
            return;
        }

        int id = idGenerator.GenerateId();
        planet = battleContext.Pool.Spawn(prefab).AddComponent<Entity>();
        planet.transform.SetParent(root.transform);
        planet.transform.position = new Vector3(x, y, 0);
        planet.Init(id, prefab, score);
        entities.Add(id, planet);

        if (OnEntityAdded != null)
            OnEntityAdded(planet.Id);
    }

    void OnHidePlanet(int x, int y)
    {
        Entity planet = GetPlanetByPosition(x, y);
        if (planet == null)
        {
            return;
        }

        battleContext.Pool.Despawn(planet.PathToPrefab, planet.gameObject);
        entities.Remove(planet.Id);

        if (OnEntityRemoved != null)
            OnEntityRemoved(planet.Id);
    }

    Entity GetPlanetByPosition(int x, int y)
    {
        foreach (var entity in entities)
        {
            if (entity.Key != shipId && entity.Value.PosX == x && entity.Value.PosY == y)
            {
                return entity.Value;
            }
        }

        return null;
    }

    public Entity GetEntity(int id)
    {
        Entity entity;
        if (entities.TryGetValue(id, out entity))
        {
            return entity;
        }
        else
        {
            throw new Exception("Cant find entity " + id);
        }
    }

    public void CreateShip()
    {
        var shipObject = battleContext.Pool.Spawn(Constants.PathToShipPrefab);
        shipId = idGenerator.GenerateId();
        var ship = shipObject.AddComponent<Entity>();
        ship.Init(shipId, Constants.PathToShipPrefab, UnityEngine.Random.Range(Constants.MinRating, Constants.MaxRating));
        entities.Add(shipId, ship);
        if (OnEntityAdded != null)
            OnEntityAdded(Ship.Id);
    }

    public void MoveShip(Vector2 dir)
    {
        Ship.Move(dir);
    }
}
