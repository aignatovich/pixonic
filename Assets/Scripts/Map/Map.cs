﻿using System;
using UnityEngine;

public partial class Map
{
    public Action<int, int, int, string> OnShowPlanet;
    public Action<int, int> OnHidePlanet;

    int seed;
    PlanetInfo[] planetsInfo;
    PlanetsCache cache;

    public Map()
    {
        seed = UnityEngine.Random.Range(0, 100000);
        cache = new PlanetsCache();
        planetsInfo = new PlanetInfo[(int)Mathf.Pow(Constants.SpecialModeZoom - 1, 2)];
        for (int i = 0; i < planetsInfo.Length; i++)
        {
            planetsInfo[i] = PlanetInfo.Empty;
        }
    }

    public void Refresh(int zoom, int shipX, int shipY, int shipScores)
    {
        bool specialMode = zoom >= Constants.SpecialModeZoom;
        int planetsLimit = specialMode ? Constants.MaxPlanetVisible : planetsInfo.Length;
        int halfZoom = zoom / 2;
        for (int i = 0; i < planetsInfo.Length; i++)
        {
            bool empty = planetsInfo[i].IsEmpty;
            bool invisible = !IsPlanetVisible(planetsInfo[i].PosX, planetsInfo[i].PosY, shipX, shipY, halfZoom);
            bool outOfLimit = i >= planetsLimit;
            if (!empty && (invisible || outOfLimit))
            {
                OnHidePlanet(planetsInfo[i].PosX, planetsInfo[i].PosY);
                planetsInfo[i] = PlanetInfo.Empty;
            }
        }

        int cacheX = 0;
        int cacheY = 0;
        int cacheZoom = 0;
        PlanetInfo[] cachedPlanets = null;
        bool cacheFounded = false;
        if (specialMode)
        {
            cacheFounded = cache.TryGetCash(shipX, shipY, zoom, out cachedPlanets, out cacheX, out cacheY, out cacheZoom);
            if (cacheFounded)
            {
                for (int i = 0; i < cachedPlanets.Length; i++)
                {
                    if (!planetsInfo[i].IsEmpty && (planetsInfo[i].PosX != cachedPlanets[i].PosX || planetsInfo[i].PosY != cachedPlanets[i].PosY))
                    {
                        OnHidePlanet(planetsInfo[i].PosX, planetsInfo[i].PosY);
                    }

                    if (!IsPlanetVisible(cachedPlanets[i].PosX, cachedPlanets[i].PosY, shipX, shipY, halfZoom))
                    {
                        cachedPlanets[i] = PlanetInfo.Empty;
                    }

                    planetsInfo[i] = cachedPlanets[i];
                }
            }
        }

        int cacheHalfZoom = cacheZoom / 2;
        for (int i = 0; i < zoom; i++)
        {
            int x = shipX + i - halfZoom;
            int y;
            if (!cacheFounded || (x < cacheX - cacheHalfZoom || x >= cacheX + cacheHalfZoom))
            {
                for (int j = 0; j < zoom; j++)
                {
                    y = shipY + j - halfZoom;
                    UpdateCell(x, y, shipScores, planetsLimit);
                }
            }

            y = shipY + i - halfZoom;
            if (!cacheFounded || (y < cacheY - cacheHalfZoom || y >= cacheY + cacheHalfZoom))
            {
                for (int j = 0; j < zoom; j++)
                {
                    x = shipX + j - halfZoom;
                    UpdateCell(x, y, shipScores, planetsLimit);
                }
            }
        }

        for (int i = 0; i < planetsInfo.Length; i++)
        {
            if (planetsInfo[i].IsEmpty)
                continue;

            UnityEngine.Random.InitState(planetsInfo[i].Seed);
            string prefab = Constants.PlanetPrefabsPathes[UnityEngine.Random.Range(0, Constants.PlanetPrefabsPathes.Length)];

            OnShowPlanet(planetsInfo[i].PosX, planetsInfo[i].PosY, planetsInfo[i].Scores, prefab);
        }

        if (specialMode)
            cache.AddCash(shipX, shipY, zoom, planetsInfo);
    }

    bool IsPlanetVisible(int planetX, int planetY, int shipX, int shipY, int halfZoom)
    {
        return planetX >= shipX - halfZoom && planetX <= shipX + halfZoom && planetY >= shipY - halfZoom && planetY <= shipY + halfZoom;
    }

    void UpdateCell(int x, int y, int shipScores, int limit)
    {
        int seedX = x * 1103515245 + 12345;
        int seedY = y * 1103515245 + 12345;
        int cellSeed = (seedX * seed + seedY) % 1000000;

        UnityEngine.Random.InitState(cellSeed);
        if (UnityEngine.Random.Range(0f, 1f) <= Constants.MinPlanetChance)
        {
            if (Array.FindIndex(planetsInfo, p => !p.IsEmpty && p.PosX == x && p.PosY == y) != -1)
            {
                return;
            }

            int maxDiff = -1;
            int maxDiffIndex = -1;
            int score = UnityEngine.Random.Range(Constants.MinRating, Constants.MaxRating);
            for (int i = 0; i < limit; i++)
            {
                int scoreDiff = Mathf.Abs(planetsInfo[i].Scores - shipScores);
                if (scoreDiff > maxDiff)
                {
                    maxDiff = scoreDiff;
                    maxDiffIndex = i;
                }
            }

            int diff = Mathf.Abs(score - shipScores);
            if (maxDiff > diff)
            {
                if (!planetsInfo[maxDiffIndex].IsEmpty)
                {
                    OnHidePlanet(planetsInfo[maxDiffIndex].PosX, planetsInfo[maxDiffIndex].PosY);
                }

                planetsInfo[maxDiffIndex].Scores = score;
                planetsInfo[maxDiffIndex].Seed = cellSeed;
                planetsInfo[maxDiffIndex].PosX = x;
                planetsInfo[maxDiffIndex].PosY = y;
            }
        }
    }
}
