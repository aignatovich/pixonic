﻿public partial class Map
{
    struct PlanetInfo
    {
        public int PosX;
        public int PosY;
        public int Scores;
        public int Seed;

        public bool IsEmpty
        {
            get
            {
                return Scores == -1;
            }
        }

        public static PlanetInfo Empty
        {
            get
            {
                PlanetInfo cellInfo = new PlanetInfo();
                cellInfo.Scores = -1;
                return cellInfo;
            }
        }
    }
}
