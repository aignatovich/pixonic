﻿using System.Collections.Generic;

public partial class Map
{
    class CacheData
    {
        public List<CachePositionData> positions;
        public PlanetInfo[] scoreDiffs;
    }
}