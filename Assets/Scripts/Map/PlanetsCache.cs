﻿using System;
using System.Collections.Generic;
using UnityEngine;

public partial class Map
{
    class PlanetsCache
    {
        List<CacheData> caches;

        public PlanetsCache()
        {
            caches = new List<CacheData>();
        }

        public void AddCash(int x, int y, int zoom, PlanetInfo[] scoreDiffs)
        {
            CacheData cache = caches.Find(c => DiffsEqual(c.scoreDiffs, scoreDiffs));
            if (cache == null)
            {
                cache = new CacheData();
                cache.scoreDiffs = new PlanetInfo[Constants.MaxPlanetVisible];
                Array.Copy(scoreDiffs, cache.scoreDiffs, Constants.MaxPlanetVisible);
                cache.positions = new List<CachePositionData>();
                CachePositionData positionParams = new CachePositionData();
                positionParams.XPos = x;
                positionParams.YPos = y;
                positionParams.MinZoom = zoom;
                positionParams.MaxZoom = zoom;
                cache.positions.Add(positionParams);
                caches.Add(cache);
            }
            else
            {
                var index = cache.positions.FindIndex(p => p.XPos == x && p.YPos == y);
                if (index != -1)
                {
                    var position = cache.positions[index];
                    if (position.MinZoom > zoom)
                    {
                        position.MinZoom = zoom;
                    }

                    if (position.MaxZoom < zoom)
                    {
                        position.MaxZoom = zoom;
                    }

                    cache.positions[index] = position;
                }
                else
                {
                    CachePositionData positionParams = new CachePositionData();
                    positionParams.XPos = x;
                    positionParams.YPos = y;
                    positionParams.MinZoom = zoom;
                    positionParams.MaxZoom = zoom;
                    cache.positions.Add(positionParams);
                }
            }
        }

        int CellsCountForCalculation(CachePositionData positionParams, int x, int y, int zoom)
        {
            int xDiff = Mathf.Abs(x - positionParams.XPos);
            int yDiff = Mathf.Abs(y - positionParams.YPos);
            return (xDiff + yDiff) * zoom + zoom * zoom - positionParams.MaxZoom * positionParams.MaxZoom;
        }

        public bool TryGetCash(int x, int y, int zoom, out PlanetInfo[] cacheScoreDiffs, out int cacheX, out int cacheY, out int cacheZoom)
        {
            int minCellsCount = int.MaxValue;
            int cacheIndex = -1;
            int posIndex = -1;
            for (int i = 0; i < caches.Count; i++)
            {
                for (int j = 0; j < caches[i].positions.Count; j++)
                {
                    if (zoom < caches[i].positions[j].MinZoom)
                        continue;

                    int cellsCount = CellsCountForCalculation(caches[i].positions[j], x, y, zoom);
                    if (cellsCount < minCellsCount)
                    {
                        minCellsCount = cellsCount;
                        cacheIndex = i;
                        posIndex = j;
                    }
                }
            }

            if (cacheIndex != -1)
            {
                cacheScoreDiffs = new PlanetInfo[Constants.MaxPlanetVisible];
                Array.Copy(caches[cacheIndex].scoreDiffs, cacheScoreDiffs, Constants.MaxPlanetVisible);
                cacheX = caches[cacheIndex].positions[posIndex].XPos;
                cacheY = caches[cacheIndex].positions[posIndex].YPos;
                cacheZoom = caches[cacheIndex].positions[posIndex].MaxZoom;
                return true;
            }

            cacheScoreDiffs = null;
            cacheX = -1;
            cacheY = -1;
            cacheZoom = -1;
            return false;
        }

        bool DiffsEqual(PlanetInfo[] a, PlanetInfo[] b)
        {
            for (int i = 0; i < a.Length; i++)
            {
                if (a[i].PosX != b[i].PosX || a[i].PosX != b[i].PosX)
                {
                    return false;
                }
            }

            return true;
        }
    }
}
