﻿public partial class Map
{
    struct CachePositionData
    {
        public int XPos;
        public int YPos;
        public int MinZoom;
        public int MaxZoom;
    }
}
